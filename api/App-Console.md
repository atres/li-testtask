App\Console
===============

Класс, упрощающий взаимодействие с консолью.




* Class name: Console
* Namespace: App







Methods
-------


### out

    mixed App\Console::out($value)

Выводит строку или массив строк на STDOUT.



* Visibility: **public**
* This method is **static**.


#### Arguments
* $value **mixed** - &lt;p&gt;array|string Данные для вывода&lt;/p&gt;



### in

    string App\Console::in()

Ожидает пользовательского ввода.



* Visibility: **public**
* This method is **static**.




### inNumber

    integer App\Console::inNumber($min, $max)

Ожидает ввода пользователем числа в указанном диапазоне.



* Visibility: **public**
* This method is **static**.


#### Arguments
* $min **mixed** - &lt;p&gt;int Минимально допустимое значение&lt;/p&gt;
* $max **mixed** - &lt;p&gt;int Максимально допустимое значение&lt;/p&gt;



### inPreg

    string App\Console::inPreg($patterns)

Ожидает ввода пользователям строки, подходящей под одно из указанных регулярных выражений.



* Visibility: **public**
* This method is **static**.


#### Arguments
* $patterns **mixed** - &lt;p&gt;string|array Массив регулярных выражений&lt;/p&gt;


