API Index
=========

* App
    * App\Figures
        * [Rook](App-Figures-Rook.md)
        * [Knight](App-Figures-Knight.md)
        * [Figure](App-Figures-Figure.md)
        * [Pawn](App-Figures-Pawn.md)
    * [Application](App-Application.md)
    * App\Storage
        * App\Storage\Vendor
            * [Redis](App-Storage-Vendor-Redis.md)
        * [RedisStorage](App-Storage-RedisStorage.md)
        * [Storage](App-Storage-Storage.md)
        * [FileStorage](App-Storage-FileStorage.md)
    * [Console](App-Console.md)
    * [Chessboard](App-Chessboard.md)

