App\Storage\FileStorage
===============

Класс для работы с файловым хранилищем.

Для хранения данных используется файл storage.json в корневой директории приложения.


* Class name: FileStorage
* Namespace: App\Storage
* Parent class: [App\Storage\Storage](App-Storage-Storage.md)





Properties
----------


### $path

    private string $path





* Visibility: **private**


Methods
-------


### __construct

    mixed App\Storage\Storage::__construct($root)

Storage constructor.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Storage\Storage](App-Storage-Storage.md)


#### Arguments
* $root **mixed** - &lt;p&gt;string Корневая директория приложения&lt;/p&gt;



### title

    string App\Storage\Storage::title()

Метод возвращает название хранилища.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Storage\Storage](App-Storage-Storage.md)




### save

    boolean App\Storage\Storage::save(\App\Chessboard $chessboard)

Метод сохраняет состояние шахматной доски в хранилище.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Storage\Storage](App-Storage-Storage.md)


#### Arguments
* $chessboard **[App\Chessboard](App-Chessboard.md)** - &lt;p&gt;Объект доски&lt;/p&gt;



### load

    array App\Storage\Storage::load()

Метод загружает состояние хранилища из файла.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Storage\Storage](App-Storage-Storage.md)



