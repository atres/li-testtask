App\Figures\Figure
===============

Абстрактный класс для классов фигур.




* Class name: Figure
* Namespace: App\Figures
* This is an **abstract** class







Methods
-------


### getTitle

    string App\Figures\Figure::getTitle()

Возвращает название шахматной фигуры.



* Visibility: **public**
* This method is **abstract**.




### getID

    string App\Figures\Figure::getID()

Возвращает идентификатор шахматной фигуры.



* Visibility: **public**
* This method is **abstract**.




### getCode

    string App\Figures\Figure::getCode()

Возвращает unicode символ для отобращения фигуры.



* Visibility: **public**
* This method is **abstract**.



