App\Storage\Storage
===============

Абстрактный класс для хранилищ.




* Class name: Storage
* Namespace: App\Storage
* This is an **abstract** class







Methods
-------


### __construct

    mixed App\Storage\Storage::__construct($root)

Storage constructor.



* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $root **mixed** - &lt;p&gt;string Корневая директория приложения&lt;/p&gt;



### title

    string App\Storage\Storage::title()

Метод возвращает название хранилища.



* Visibility: **public**
* This method is **abstract**.




### save

    boolean App\Storage\Storage::save(\App\Chessboard $chessboard)

Метод сохраняет состояние шахматной доски в хранилище.



* Visibility: **public**
* This method is **abstract**.


#### Arguments
* $chessboard **[App\Chessboard](App-Chessboard.md)** - &lt;p&gt;Объект доски&lt;/p&gt;



### load

    array App\Storage\Storage::load()

Метод загружает состояние хранилища из файла.



* Visibility: **public**
* This method is **abstract**.



