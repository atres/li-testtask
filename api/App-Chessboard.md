App\Chessboard
===============

Класс предоставляет функционал для работы с шахматной доской.




* Class name: Chessboard
* Namespace: App



Constants
----------


### BOARD_SIZE

    const BOARD_SIZE = 64





### BOARD_COLUMNS

    const BOARD_COLUMNS = 'abcdefghijklmnopqrstuvwxyz'





### BOARD_FIGURES

    const BOARD_FIGURES = array(\App\Figures\Rook::class, \App\Figures\Knight::class, \App\Figures\Pawn::class)





Properties
----------


### $boardCount

    public integer $boardCount





* Visibility: **public**


### $figures

    private array<mixed,\App\Figures\Figure> $figures = array()





* Visibility: **private**


### $board

    private array $board = array()





* Visibility: **private**


Methods
-------


### __construct

    mixed App\Chessboard::__construct()

Chessboard constructor.



* Visibility: **public**




### board

    array App\Chessboard::board()

Возвращает состояние доски для последующего сохранения.

Объекты фигур заменены на их идентификаторы.

* Visibility: **public**




### load

    mixed App\Chessboard::load($data)

Загружает состояние шахматной доски.

Данные должны быть в формате, предоставляемом методом ::board().

* Visibility: **public**


#### Arguments
* $data **mixed** - &lt;p&gt;array Состояние шахматной доски.&lt;/p&gt;



### display

    mixed App\Chessboard::display()

Выводит состояние шахматной доски на консоль.



* Visibility: **public**




### addFigure

    mixed App\Chessboard::addFigure(\Closure $cb)

Процесс добавления фигуры на доску.



* Visibility: **public**


#### Arguments
* $cb **Closure** - &lt;p&gt;Функция, вызываемая при добавлении фигуры&lt;/p&gt;



### deleteFigure

    mixed App\Chessboard::deleteFigure()

Процесс удаления фигуры с доски.



* Visibility: **public**




### moveFigure

    mixed App\Chessboard::moveFigure()

Процесс перемещения фигуры на доске.



* Visibility: **public**




### checkPosition

    boolean App\Chessboard::checkPosition($position)

Проверяет валидность указанной позиции.



* Visibility: **private**


#### Arguments
* $position **mixed** - &lt;p&gt;string Позиция на шахмотной доске&lt;/p&gt;



### place

    mixed App\Chessboard::place($position, $figure)

Размещает фигуру на доске.



* Visibility: **private**


#### Arguments
* $position **mixed** - &lt;p&gt;string Позиция на шахматной доске&lt;/p&gt;
* $figure **mixed** - &lt;p&gt;Figure|int Фигура&lt;/p&gt;



### remove

    mixed App\Chessboard::remove($position)

Удаляет фигуру с доски.



* Visibility: **private**


#### Arguments
* $position **mixed** - &lt;p&gt;string Позиция на шахматной доске.&lt;/p&gt;



### move

    mixed App\Chessboard::move($position, $newPosition)

Перемещает фигуру по доске.



* Visibility: **private**


#### Arguments
* $position **mixed** - &lt;p&gt;string Позиция на шахматной доске.&lt;/p&gt;
* $newPosition **mixed** - &lt;p&gt;string Новая позиция.&lt;/p&gt;


