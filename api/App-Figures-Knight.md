App\Figures\Knight
===============

Класс шахматной фигуры &quot;Конь&quot;.




* Class name: Knight
* Namespace: App\Figures
* Parent class: [App\Figures\Figure](App-Figures-Figure.md)







Methods
-------


### getTitle

    string App\Figures\Figure::getTitle()

Возвращает название шахматной фигуры.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Figures\Figure](App-Figures-Figure.md)




### getID

    string App\Figures\Figure::getID()

Возвращает идентификатор шахматной фигуры.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Figures\Figure](App-Figures-Figure.md)




### getCode

    string App\Figures\Figure::getCode()

Возвращает unicode символ для отобращения фигуры.



* Visibility: **public**
* This method is **abstract**.
* This method is defined by [App\Figures\Figure](App-Figures-Figure.md)



