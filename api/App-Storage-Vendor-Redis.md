App\Storage\Vendor\Redis
===============

Raw redis wrapper, all the commands are passed as-is
More information and usage examples could be found on https://github.com/ziogas/PHP-Redis-implementation

Based on http://redis.io/topics/protocol


* Class name: Redis
* Namespace: App\Storage\Vendor



Constants
----------


### INTEGER

    const INTEGER = ':'





### INLINE

    const INLINE = '+'





### BULK

    const BULK = '$'





### MULTIBULK

    const MULTIBULK = '*'





### ERROR

    const ERROR = '-'





### NL

    const NL = "\r\n"





Properties
----------


### $handle

    private mixed $handle = false





* Visibility: **private**


### $host

    private mixed $host





* Visibility: **private**


### $port

    private mixed $port





* Visibility: **private**


### $silent_fail

    private mixed $silent_fail





* Visibility: **private**


### $commands

    private mixed $commands = array()





* Visibility: **private**


### $timeout

    private mixed $timeout = 30





* Visibility: **private**


### $connect_timeout

    private mixed $connect_timeout = 3





* Visibility: **private**


### $force_reconnect

    private mixed $force_reconnect = false





* Visibility: **private**


### $last_used_command

    private mixed $last_used_command = ''





* Visibility: **private**


### $error_function

    private mixed $error_function = null





* Visibility: **private**


Methods
-------


### __construct

    mixed App\Storage\Vendor\Redis::__construct($host, $port, $silent_fail, $timeout)





* Visibility: **public**


#### Arguments
* $host **mixed**
* $port **mixed**
* $silent_fail **mixed**
* $timeout **mixed**



### connect

    mixed App\Storage\Vendor\Redis::connect($host, $port, $silent_fail, $timeout)





* Visibility: **public**


#### Arguments
* $host **mixed**
* $port **mixed**
* $silent_fail **mixed**
* $timeout **mixed**



### reconnect

    mixed App\Storage\Vendor\Redis::reconnect()





* Visibility: **public**




### __destruct

    mixed App\Storage\Vendor\Redis::__destruct()





* Visibility: **public**




### commands

    mixed App\Storage\Vendor\Redis::commands()





* Visibility: **public**




### cmd

    mixed App\Storage\Vendor\Redis::cmd()





* Visibility: **public**




### set

    mixed App\Storage\Vendor\Redis::set()





* Visibility: **public**




### get

    mixed App\Storage\Vendor\Redis::get($line)





* Visibility: **public**


#### Arguments
* $line **mixed**



### get_len

    mixed App\Storage\Vendor\Redis::get_len()





* Visibility: **public**




### set_force_reconnect

    mixed App\Storage\Vendor\Redis::set_force_reconnect($flag)





* Visibility: **public**


#### Arguments
* $flag **mixed**



### get_response

    mixed App\Storage\Vendor\Redis::get_response()





* Visibility: **private**




### inline_response

    mixed App\Storage\Vendor\Redis::inline_response()





* Visibility: **private**




### integer_response

    mixed App\Storage\Vendor\Redis::integer_response()





* Visibility: **private**




### error_response

    mixed App\Storage\Vendor\Redis::error_response()





* Visibility: **private**




### bulk_response

    mixed App\Storage\Vendor\Redis::bulk_response()





* Visibility: **private**




### multibulk_response

    mixed App\Storage\Vendor\Redis::multibulk_response()





* Visibility: **private**




### exec

    mixed App\Storage\Vendor\Redis::exec()





* Visibility: **private**




### read_bulk_response

    mixed App\Storage\Vendor\Redis::read_bulk_response($tmp)





* Visibility: **private**


#### Arguments
* $tmp **mixed**



### set_error_function

    mixed App\Storage\Vendor\Redis::set_error_function($func)





* Visibility: **public**


#### Arguments
* $func **mixed**


