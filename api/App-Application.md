App\Application
===============

Основной класс приложениия.

Отвечает за инициализацию объектов и управление процессами приложения.


* Class name: Application
* Namespace: App



Constants
----------


### STORAGE

    const STORAGE = array(\App\Storage\FileStorage::class, \App\Storage\RedisStorage::class)





Properties
----------


### $APPLICATION_ROOT

    public string $APPLICATION_ROOT





* Visibility: **public**
* This property is **static**.


### $chessboard

    private \App\Chessboard $chessboard





* Visibility: **private**


### $actions

    private array $actions = array()





* Visibility: **private**


### $storage

    private array<mixed,\App\Storage\Storage> $storage = array()





* Visibility: **private**


Methods
-------


### __construct

    mixed App\Application::__construct()

Application constructor.



* Visibility: **public**




### loop

    mixed App\Application::loop(\Closure $cb)

Метод обеспечивает цикл работы приложения.



* Visibility: **public**


#### Arguments
* $cb **Closure** - &lt;p&gt;Функция, вызываемая при добавлении фигуры&lt;/p&gt;



### showActions

    mixed App\Application::showActions()

Выводит список доступных на данный момент действий.



* Visibility: **private**




### save

    mixed App\Application::save()

Сохраняет текущее состояние шахматной доски в хранилище.



* Visibility: **private**




### load

    mixed App\Application::load()

Загружает состояние шахматной доски из хранилища.



* Visibility: **private**




### autoload

    mixed App\Application::autoload()

Метод для автоматической загрузки.



* Visibility: **private**



