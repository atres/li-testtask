<?php

namespace App;

require 'figures/Rook.php';

use App\Figures\Figure;
use App\Figures\Knight;
use App\Figures\Pawn;
use App\Figures\Rook;

/**
 * Класс предоставляет функционал для работы с шахматной доской.
 *
 * @package App
 */
class Chessboard {
    /**
     * @const integer Размер доски
     */
    const BOARD_SIZE = 64;

    /**
     * @const string Символы для именования столбцов.
     */
    const BOARD_COLUMNS = 'abcdefghijklmnopqrstuvwxyz';

    /**
     * @const Figure[] Доступные фигуры.
     */
    const BOARD_FIGURES = [Rook::class, Knight::class, Pawn::class];

    /**
     * @var int Количество фигур на доске.
     */
    public $boardCount = 0;

    /**
     * @var \App\Figures\Figure[] Объекты доступных фигур.
     */
    private $figures = array();

    /**
     * @var array Данные о размещённых на доске фигурах.
     */
    private $board = array();

    /**
     * Chessboard constructor.
     */
    public function __construct() {
        foreach (self::BOARD_FIGURES as $class) {
            $this->figures[] = new $class;
        }
        for($i = 0; $i < sqrt(self::BOARD_SIZE); $i++) {
            $row = [];
            for($n = 0; $n < sqrt(self::BOARD_SIZE); $n++) {
                $row[$n] = true;
            }
            $this->board[self::BOARD_COLUMNS[$i]] = $row;
        }
    }

    /**
     * Возвращает состояние доски для последующего сохранения.
     * Объекты фигур заменены на их идентификаторы.
     *
     * @return array Состояние шахматной доски.
     */
    public function board() {
        $board = [];
        foreach ($this->board as $key => $val) {
            $board[$key] = [];
            foreach ($val as $k => $v) {
                if (!is_bool($v)) {
                    $v = $v->getID();
                }
                $board[$key][$k] = $v;
            }
        }
        return $board;
    }

    /**
     * Загружает состояние шахматной доски.
     * Данные должны быть в формате, предоставляемом методом ::board().
     *
     * @param $data array Состояние шахматной доски.
     */
    public function load($data) {
        $indexes = [];
        foreach ($this->figures as $index => $figure) {
            $indexes[$figure->getID()] = $index;
        }

        $board = [];
        $this->boardCount = 0;
        foreach ($data as $key => $val) {
            $board[$key] = [];
            foreach ($val as $k => $v) {
                if (!is_bool($v)) {
                    $v = $this->figures[$indexes[$v]];
                    $this->boardCount++;
                }
                $board[$key][$k] = $v;
            }
        }

        $this->board = $board;
    }

    /**
     * Выводит состояние шахматной доски на консоль.
     */
    public function display() {
        $result = [];
        $nrow = '  ';
        foreach ($this->board as $key => $val) {
            $nrow .= $key . ' ';
        }
        $result[] = $nrow;
        for ($i = sqrt(self::BOARD_SIZE) -  1; $i >= 0; $i--) {
            $row = ($i + 1) . '|';
            foreach ($this->board as $key => $val) {
                if (is_bool($val[$i])) {
                    $row .= ' |';
                } else {
                    $row .= $val[$i]->getCode() . '|';
                }
            }
            $row .= $i + 1;
            $result[] = $row;
        }
        $result[] = $nrow;
        Console::out($result);
    }

    /**
     * Процесс добавления фигуры на доску.
     *
     * @param \Closure $cb Функция, вызываемая при добавлении фигуры
     */
    public function addFigure(\Closure $cb) {
        Console::out('-------------------');
        Console::out('Выберите фигуру для добавления:');

        foreach ($this->figures as $index => $figure) {
            Console::out(($index + 1) . '. ' . $figure->getTitle());
        }

        Console::out((count($this->figures) + 1) . '. Назад');
        $index = Console::inNumber(1, count($this->figures) + 1) - 1;
        if (isset($this->figures[$index])) {
            do {
                Console::out('Укажите клетку для размещения (например b6):');
                $position = strtolower(Console::inPreg(['/^[A-Z][0-9]$/i', '/^[0-9][A-Z]/i']));
                if (preg_match('/^[0-9][A-Z]$/i', $position)) {
                    $position = $position[1] . $position[0];
                }
                $square = $this->checkPosition($position);
                if (false === $square) {
                    Console::out('Указанной клетки не существует!');
                } else if (!is_bool($square)) {
                    Console::out('Клетка занята другой фигурой!');
                } else {
                    $this->place($position, $index);
                    if ($cb) call_user_func($cb, $this->figures[$index]);
                    return;
                }
            } while (true);
        }
    }

    /**
     * Процесс удаления фигуры с доски.
     */
    public function deleteFigure() {
        Console::out('-------------------');
        do {
            Console::out('Укажите клетку для удаления:');
            $position = strtolower(Console::inPreg(['/^[A-Z][0-9]$/i', '/^[0-9][A-Z]/i']));
            if (preg_match('/^[0-9][A-Z]$/i', $position)) {
                $position = $position[1] . $position[0];
            }
            $square = $this->checkPosition($position);
            if (false === $square) {
                Console::out('Указанной клетки не существует!');
            } else if (is_bool($square)) {
                Console::out('На указанной клетке нет фигуры!');
            } else {
                $this->remove($position);
                return;
            }
        } while (true);
    }

    /**
     * Процесс перемещения фигуры на доске.
     */
    public function moveFigure() {
        Console::out('-------------------');
        do {
            Console::out('Укажите клетку для перемещения:');
            $position = strtolower(Console::inPreg(['/^[A-Z][0-9]$/i', '/^[0-9][A-Z]/i']));
            if (preg_match('/^[0-9][A-Z]$/i', $position)) {
                $position = $position[1] . $position[0];
            }
            $square = $this->checkPosition($position);
            if (false === $square) {
                Console::out('Указанной клетки не существует!');
            } else if (is_bool($square)) {
                Console::out('На указанной клетке нет фигуры!');
            } else {
                Console::out('Укажите куда переместить фигуру:');
                $newPosition = strtolower(Console::inPreg(['/^[A-Z][0-9]$/i', '/^[0-9][A-Z]/i']));
                if (preg_match('/^[0-9][A-Z]$/i', $newPosition)) {
                    $newPosition = $newPosition[1] . $newPosition[0];
                }
                $newSquare = $this->checkPosition($newPosition);
                if (false === $newSquare) {
                    Console::out('Указанной клетки не существует!');
                } else if (!is_bool($newSquare)) {
                    Console::out('Указанная клетка занята!');
                } else {
                    $this->move($position, $newPosition);
                    return;
                }
            }
        } while (true);
    }

    /**
     * Проверяет валидность указанной позиции.
     *
     * @param $position string Позиция на шахмотной доске
     * @return bool
     */
    private function checkPosition($position) {
        if (isset($this->board[$position[0]][$position[1] - 1])) {
            return $this->board[$position[0]][$position[1] - 1];
        }
        return false;
    }

    /**
     * Размещает фигуру на доске.
     *
     * @param $position string Позиция на шахматной доске
     * @param $figure Figure|int Фигура
     */
    private function place($position, $figure) {
        if (is_int($figure)) $figure = $this->figures[$figure];
        $this->board[$position[0]][$position[1] - 1] = $figure;
        $this->boardCount++;
    }

    /**
     * Удаляет фигуру с доски.
     *
     * @param $position string Позиция на шахматной доске.
     */
    private function remove($position) {
        $this->board[$position[0]][$position[1] - 1] = true;
        $this->boardCount--;
    }

    /**
     * Перемещает фигуру по доске.
     *
     * @param $position string Позиция на шахматной доске.
     * @param $newPosition string Новая позиция.
     */
    private function move($position, $newPosition) {
        $figure = $this->board[$position[0]][$position[1] - 1];
        $this->remove($position);
        $this->place($newPosition, $figure);
    }
}