<?php

namespace App\Figures;

/**
 * Абстрактный класс для классов фигур.
 *
 * @package App\Figures
 */
abstract class Figure {
    /**
     * Возвращает название шахматной фигуры.
     * @return string Название фигуры
     */
    abstract public function getTitle();

    /**
     * Возвращает идентификатор шахматной фигуры.
     * @return string Идентификатор
     */
    abstract public function getID();

    /**
     * Возвращает unicode символ для отобращения фигуры.
     * @return string Код фигуры
     */
    abstract public function getCode();
}