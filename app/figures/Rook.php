<?php

namespace App\Figures;

/**
 * Класс шахматной фигуры "Ладья".
 *
 * @package App\Figures
 */
class Rook extends Figure {
    /**
     * Возвращает название шахматной фигуры.
     * @return string Название фигуры
     */
    public function getTitle() {
        return 'Ладья';
    }

    /**
     * Возвращает идентификатор шахматной фигуры.
     * @return string Идентификатор
     */
    public function getID() {
        return 'rook';
    }

    /**
     * Возвращает unicode символ для отобращения фигуры.
     * @return string Код фигуры
     */
    public function getCode() {
        return "\u{265C}";
    }
}