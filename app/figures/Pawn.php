<?php

namespace App\Figures;

/**
 * Класс шахматной фигуры "Пешка".
 *
 * @package App\Figures
 */
class Pawn extends Figure {
    /**
     * Возвращает название шахматной фигуры.
     * @return string Название фигуры
     */
    public function getTitle() {
        return 'Пешка';
    }

    /**
     * Возвращает идентификатор шахматной фигуры.
     * @return string Идентификатор
     */
    public function getID() {
        return 'pawn';
    }

    /**
     * Возвращает unicode символ для отобращения фигуры.
     * @return string Код фигуры
     */
    public function getCode() {
        return "\u{265F}";
    }
}