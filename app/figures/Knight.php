<?php

namespace App\Figures;

/**
 * Класс шахматной фигуры "Конь".
 *
 * @package App\Figures
 */
class Knight extends Figure {
    /**
     * Возвращает название шахматной фигуры.
     * @return string Название фигуры
     */
    public function getTitle() {
        return 'Конь';
    }

    /**
     * Возвращает идентификатор шахматной фигуры.
     * @return string Идентификатор
     */
    public function getID() {
        return 'knight';
    }

    /**
     * Возвращает unicode символ для отобращения фигуры.
     * @return string Код фигуры
     */
    public function getCode() {
        return "\u{265E}";
    }
}