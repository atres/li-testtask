<?php

namespace App\Storage;

use App\Chessboard;

/**
 * Класс для работы с файловым хранилищем.
 * Для хранения данных используется файл storage.json в корневой директории приложения.
 *
 * @package App\Storage
 */
class FileStorage extends Storage {
    /**
     * @var string Абсолютный путь к файлу хранилища
     */
    private $path;

    /**
     * FileStorage constructor.
     *
     * @param string $root Путь к корневой директории приложения
     */
    public function __construct($root) {
        $this->path = $root . 'storage.json';
    }

    /**
     * Возвращает название хранилища.
     *
     * @return string Название хранилища
     */
    public function title() {
        return 'Файловое хранилище';
    }

    /**
     * Сохраняет состояние шахматной доски.
     *
     * @param Chessboard $chessboard Объект доски
     * @return bool Результат сохранения
     */
    public function save(Chessboard $chessboard) {
        if (file_put_contents($this->path, json_encode($chessboard->board()))) {
            return true;
        }
        return false;
    }

    /**
     * Загружает состояние шахматной доски.
     *
     * @return bool|array Результат загрузки
     */
    public function load() {
        if (file_exists($this->path)) {
            $data = file_get_contents($this->path);
            if ($data) return json_decode($data);
        }
        return false;
    }
}