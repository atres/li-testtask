<?php

namespace App\Storage;

use App\Chessboard;
use App\Storage\Vendor\Redis;

/**
 * Класс хранилища для работы с Redis.
 *
 * @package App\Storage
 */
class RedisStorage extends Storage {
    /**
     * @const string Хост к серверу Redis
     */
    const REDIS_HOST = '127.0.0.1';

    /**
     * @const int Номер порта сервера
     */
    const REDIS_PORT = 6379;

    /**
     * @var Redis Клиентская библиотека
     */
    private $client;

    /**
     * RedisStorage constructor.
     * @param string $root Путь к корневой директории приложения
     */
    public function __construct($root) {
        $this->client = new Redis(self::REDIS_HOST, self::REDIS_PORT);
    }

    /**
     * Возвращает название хранилища.
     *
     * @return string Название хранилища
     */
    public function title() {
        return 'Redis хранилище';
    }

    /**
     * Сохраняет состояние шахматной доски.
     *
     * @param Chessboard $chessboard Объект доски
     * @return bool Результат сохранения
     */
    public function save(Chessboard $chessboard) {
        $this->client->cmd('SET', 'chessboard', json_encode($chessboard->board()))->set();
        return true;
    }

    /**
     * Загружает состояние шахматной доски.
     *
     * @return bool|array Результат загрузки
     */
    public function load() {
        if ($data = $this->client->cmd('GET', 'chessboard')->get()) {
            return json_decode($data);
        }
        return false;
    }
}