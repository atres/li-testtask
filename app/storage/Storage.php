<?php

namespace App\Storage;

use App\Chessboard;

/**
 * Абстрактный класс для хранилищ.
 *
 * @package App\Storage
 */
abstract class Storage {
    /**
     * Storage constructor.
     * @param $root string Корневая директория приложения
     */
    abstract public function __construct($root);

    /**
     * Метод возвращает название хранилища.
     *
     * @return string Название хранилища
     */
    abstract public function title();

    /**
     * Метод сохраняет состояние шахматной доски в хранилище.
     *
     * @param Chessboard $chessboard Объект доски
     * @return bool Результат сохранения
     */
    abstract public function save(Chessboard $chessboard);

    /**
     * Метод загружает состояние хранилища из файла.
     *
     * @return array
     */
    abstract public function load();
}