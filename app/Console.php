<?php

namespace App;

/**
 * Класс, упрощающий взаимодействие с консолью.
 *
 * @package App
 */
class Console {
    /**
     * Выводит строку или массив строк на STDOUT.
     *
     * @param $value array|string Данные для вывода
     */
    public static function out($value) {
        if (is_array($value)) $value = implode("\n", $value);
        echo($value . "\n");
    }

    /**
     * Ожидает пользовательского ввода.
     *
     * @return string Полученные данные
     */
    public static function in() {
        do {
            echo('> ');
            $input = trim(fgets(STDIN));
        } while (!$input);
        return $input;
    }

    /**
     * Ожидает ввода пользователем числа в указанном диапазоне.
     *
     * @param $min int Минимально допустимое значение
     * @param $max int Максимально допустимое значение
     * @return int Полученное число
     */
    public static function inNumber($min, $max) {
        do {
            echo('> ');
            $input = (int) round(intval(trim(fgets(STDIN))));
            if (is_int($input) && $input >= $min && $input <= $max) return $input;
        } while (true);
    }

    /**
     * Ожидает ввода пользователям строки, подходящей под одно из указанных регулярных выражений.
     *
     * @param $patterns string|array Массив регулярных выражений
     * @return string Полученные данные
     */
    public static function inPreg($patterns) {
        if (!is_array($patterns)) $patterns = array($patterns);
        do {
            echo('> ');
            $input = trim(fgets(STDIN));
            foreach ($patterns as $pattern) {
                if (preg_match($pattern, $input)) return $input;
            }
        } while (true);
    }
}