<?php

namespace App;

use App\Storage\FileStorage;
use App\Storage\RedisStorage;
use App\Storage\Storage;

/**
 * Основной класс приложениия.
 * Отвечает за инициализацию объектов и управление процессами приложения.
 *
 * @package App
 */
class Application {
    /**
     * @var string Корневая директория приложения
     */
    public static $APPLICATION_ROOT;

    /**
     * @const array Массив доступных хранилищ
     */
    const STORAGE = [FileStorage::class, RedisStorage::class];

    /**
     * @var Chessboard Объект для работы с шахматной доской
     */
    private $chessboard;

    /**
     * @var array Доступные на данный момент действия
     */
    private $actions = [];

    /**
     * @var Storage[] Доступные объекты хранилищ
     */
    private $storage = [];

    /**
     * Application constructor.
     */
    public function __construct() {
        $this->autoload();
        $this->chessboard = new Chessboard();

        foreach (self::STORAGE as $class) {
            $this->storage[] = new $class(self::$APPLICATION_ROOT);
        }
    }

    /**
     * Метод обеспечивает цикл работы приложения.
     *
     * @param \Closure $cb Функция, вызываемая при добавлении фигуры
     */
    public function loop(\Closure $cb = null) {
        $finished = false;
        do {
            $this->chessboard->display();
            $this->showActions();
            $index = Console::inNumber(1, count($this->actions));

            switch ($this->actions[$index]) {
                case 'add':
                    $this->chessboard->addFigure($cb);
                    break;
                case 'del':
                    $this->chessboard->deleteFigure();
                    break;
                case 'mov':
                    $this->chessboard->moveFigure();
                    break;
                case 'save':
                    $this->save();
                    break;
                case 'load':
                    $this->load();
                    break;
                case 'exit':
                    $finished = true;
                    break;
            }
        } while (!$finished);
    }

    /**
     * Выводит список доступных на данный момент действий.
     */
    private function showActions() {
        Console::out('-------------------');
        Console::out('Выберите действие:');

        $index = 1;
        $this->actions = [];

        if ($this->chessboard->boardCount < Chessboard::BOARD_SIZE) {
            Console::out($index . '. Добавить фигуру на доску');
            $this->actions[$index++] = 'add';
        }

        if ($this->chessboard->boardCount > 0) {
            Console::out($index . '. Удалить фигуру');
            $this->actions[$index++] = 'del';
            Console::out($index . '. Переместить фигуру');
            $this->actions[$index++] = 'mov';
        }

        if (count($this->storage)) {
            Console::out($index . '. Сохранить состояние');
            $this->actions[$index++] = 'save';
            Console::out($index . '. Загрузить состояние');
            $this->actions[$index++] = 'load';
        }

        Console::out($index . '. Выйти');
        $this->actions[$index] = 'exit';
    }

    /**
     * Сохраняет текущее состояние шахматной доски в хранилище.
     */
    private function save() {
        Console::out('-------------------');
        Console::out('Выберите хранилище для сохранения:');

        foreach ($this->storage as $key => $storage) {
            Console::out(($key + 1) . '. ' . $storage->title());
        }
        Console::out((count($this->storage) + 1) . '. Назад');

        $index = Console::inNumber(1, count($this->storage) + 1) - 1;
        if (isset($this->storage[$index])) {
            if ($this->storage[$index]->save($this->chessboard)) {
                Console::out('Данные сохранены!');
            } else {
                Console::out('Ошибка при сохранении данных!');
            }
        }
    }

    /**
     * Загружает состояние шахматной доски из хранилища.
     */
    private function load() {
        Console::out('-------------------');
        Console::out('Выберите хранилище для загрузки:');

        foreach ($this->storage as $key => $storage) {
            Console::out(($key + 1) . '. ' . $storage->title());
        }
        Console::out((count($this->storage) + 1) . '. Назад');

        $index = Console::inNumber(1, count($this->storage) + 1) - 1;
        if (isset($this->storage[$index])) {
            $data = $this->storage[$index]->load();
            if ($data) {
                $this->chessboard->load($data);
            } else {
                Console::out('Ошибка при загрузке данных!');
            }
        }
    }

    /**
     * Метод для автоматической загрузки.
     */
    private function autoload() {
        self::$APPLICATION_ROOT = substr(__DIR__, 0, -3);
        spl_autoload_register(function($class) {
            $class = explode('\\', $class);
            $className = array_pop($class);
            $path = Application::$APPLICATION_ROOT . strtolower(implode('/', $class)) . '/' . $className . '.php';
            require $path;
        });
    }
}