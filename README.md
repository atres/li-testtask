# Local Internet Test Task (PHP)

При написании использовалась версия PHP 7.1 и Redis 4.
Для работы с Redis применяется библиотека [PHP Redis implementation v2.1](https://github.com/ziogas/PHP-Redis-implementation).

Вызов производится через файл `index.php`, который создаёт объект `App\Application`
с последующим вызовом метода `Application::loop()`.
В качестве аргумента методу `loop()` можно передать анонимную функцию, которая
будет вызываться при добавлении фигуры на шахматную доску.

По умолчанию подключение к серверу Redis производится по адресу `127.0.0.1:6379`.
Хост и порт можно изменить в файле `app/storage/RedisStorage.php`.
